<?php
/*
 * General authentication utilities
 */

$password = trim(file_get_contents(getenv("PASSWORD_FILE")));
$cookie = hash("sha256", $password . date("Ymd"));
function is_authenticated() {
    global $cookie;
    if (!array_key_exists('auth_token', $_COOKIE))
        return false;
    return $cookie === $_COOKIE['auth_token'];
}

function show_auth_page() {
    header('Location: authenticate.php');
    exit(302);
}

function try_authenticate($attempt) {
    global $password, $cookie;
    if ($attempt === $password) {
        setcookie("auth_token", $cookie);
        return true;
    }
    return false;
}
?>
