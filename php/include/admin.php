<?php
/*
 * Administrative functions
 */

function something() {
    global $cookie;
    if (!array_key_exists('auth_token', $_COOKIE))
        return false;
    return $cookie === $_COOKIE['auth_token'];
}

?>
