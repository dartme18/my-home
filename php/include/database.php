<?

function tableExists($db, $table) {
    // Run it in try/catch in case PDO is in ERRMODE_EXCEPTION.
    try {
        $result = $db->query("select 1 from $table limit 1");
    } catch (Exception $e) {
        return false;
    }
    return $result !== false;
}

function addPictureTable($db) {
    if (tableExists($db, 'picture')) {
        return;
    }
    error_log("Creating picture table\n");
    $db->exec('create table picture (name, file, extension, description);');
}

function changelog($db) {
    $db->beginTransaction();
	if (!tableExists($db, 'changelog')){
        error_log("Creating changelog table\n");
        $db->exec('create table changelog (name, timestamp);');
    }
    addPictureTable($db);
    $db->commit();
}

function get_connection() {
    $sqlite_file = '/data/myhome.db';
    try {
        $db = new PDO("sqlite:$sqlite_file");
    } catch (Exception $e) {
        error_log("Got exception connecting: " . var_export($e, true) . "\n");
        error_log("db is " . var_export($db, true) . "\n");
        return null;
    }
    $lock = "/tmp/myhome.php.lock" ;
    error_log("Checking for lock file: " . $lock . "\n");
    if(!is_file($lock)) {
        error_log("  File didn't exist. running changelog and creating lock.\n");
        changelog($db);
        touch($lock);
    } else {
        error_log("  Lock exists.\n");
    }
    return $db;
}

?>
