<?php

require('include/database.php');
require('include/settings.php');

function get_picture_file($file_num, $extension) {
    global $deploy_path;
    return "/php$deploy_path/images/$file_num.$extension";
}

function get_picture_link($picture) {
    error_log("Getting picture link from picture " . var_export($picture, true) . "\n");
    if ($picture === null) {
        return null;
    }
    return "images/$picture[file].$picture[extension]";
}

function get_pictures() {
    $db = get_connection();
    $result = $db->query('select name, file from picture;');
    return $result;
}

function show_all_pictures() {
    echo '<div class="pages"><h3>Pictures</h3><hr>';
    foreach (get_pictures() as $picture) {
        error_log("picture is " . var_export($picture, true) . "\n");
        require('include/picture_template.php');
    }
    echo '</div>';
}

function upload_error_code_to_string($code)
{
    switch ($code) {
    case UPLOAD_ERR_OK: return null;
    case UPLOAD_ERR_INI_SIZE: return "The uploaded file exceeds the upload_max_filesize directive in php.ini";
    case UPLOAD_ERR_FORM_SIZE: return "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
    case UPLOAD_ERR_PARTIAL: return "The uploaded file was only partially uploaded";
    case UPLOAD_ERR_NO_FILE: return "No file was uploaded";
    case UPLOAD_ERR_NO_TMP_DIR: return "Missing a temporary folder";
    case UPLOAD_ERR_CANT_WRITE: return "Failed to write file to disk";
    case UPLOAD_ERR_EXTENSION: return "File upload stopped by extension";
    default: return "Unknown Error";
    }
}

function add_picture($name, $description) {
    if ($name === null || strlen($name) === 0) {
        return "Name required";
    }
    $error = upload_error_code_to_string($_FILES['picture']['error']);
    if ($error !== null) {
        return $error;
    }
    if (!is_uploaded_file($_FILES['picture']['tmp_name'])) {
        error_log("File " . $_FILES['picture']['tmp_name'] . " is not uploaded? Trying to trick me!?\n");
        return "Why u try to trick me?";
    }
    $originalExtension = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
    /* Whitelist for file extensions. */
    if ($originalExtension === null || !preg_match('/^[a-zA-Z0-9_][a-zA-Z0-9_]?[a-zA-Z0-9_]?$/', $originalExtension)) {
        return "Invalid Extension. The extension must be 1-3 characters, numbers, letters, and underscore only.";
    }
    if (null === ($db = get_connection())) {
        return "Internal Error Connecting";
    }
    $file_number = $db->query("select max(file) from picture;");
    $fetch = $file_number->fetchColumn();
    if ($fetch === null) {
        $file_number = 0;
    } else {
        $file_number = $fetch + 1;
    }
    $new_file = get_picture_file($file_number, $originalExtension);
    error_log("Saving file to $new_file\n");
    if (file_exists($new_file)) {
        error_log("File $new_file already exists!\n");
        return "Internal Error Preparing";
    }
    if (!move_uploaded_file($_FILES['picture']['tmp_name'], "$new_file")) {
        return 'Internal Error Saving';
    }
    $st = $db->prepare("insert into picture (name, file, extension, description) values (?, ?, ?, ?);");
    if (!$st->execute(array($name, $file_number, $originalExtension, $description))) {
        if (!unlink("$new_file")) {
            error_log("Error deleting $new_file .\n");
        }
        error_log("Error executing query. deleted new file $new_file\n");
        return 'Internal Error Writing';
    }
    return null;
}

function edit_picture($picture_data) {
    error_log("Editing picture " . var_export($picture_data['picturenum'], true) . "\n");
    if ($picture_data['picturenum'] === null || strlen($picture_data['picturenum']) === 0) {
        return 'Invalid picture to edit';
    }
    $old_picture = get_picture($picture_data['picturenum']);
    if ($old_picture === null) {
        return "Invalid picture to edit";
    }
    if (null === ($db = get_connection())) {
        return "Internal Error Connecting";
    }
    $file_number=$old_picture['file'];
    $new_extension=$old_picture['extension'];
    if (isset($_FILES) && isset($_FILES['picture']) && $_FILES['picture']['error'] !== 4) {
        error_log("file uploaded\n");
        $error = upload_error_code_to_string($_FILES['picture']['error']);
        if ($error !== null) {
            return $error;
        }
        if (!is_uploaded_file($_FILES['picture']['tmp_name'])) {
            error_log("File " . $_FILES['picture']['tmp_name'] . " is not uploaded? Trying to trick me!?\n");
            return "Why u try to trick me?";
        }
        $originalExtension = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
        /* Whitelist for file extensions. */
        if ($originalExtension === null || !preg_match('/^[a-zA-Z0-9_][a-zA-Z0-9_]?[a-zA-Z0-9_]?$/', $originalExtension)) {
            return "Invalid Extension. The extension must be 1-3 characters, numbers, letters, and underscore only.";
        }
        $file_number = $db->query("select max(file) from picture;");
        $new_extension = $originalExtension;
        error_log("new extension is " . var_export($new_extension, true) . " original is " . var_export($originalExtension, true) . "\n");
        $new_file=$file_number;
        $fetch = $file_number->fetchColumn();
        if ($fetch === null) {
            $file_number = 0;
        } else {
            $file_number = $fetch + 1;
        }
        $new_file = get_picture_file($file_number,$originalExtension);
        error_log("Saving new edit file to $new_file\n");
        if (file_exists($new_file)) {
            error_log("File $new_file already exists!\n");
            return "Internal Error Preparing";
        }
        if (!move_uploaded_file($_FILES['picture']['tmp_name'], "$new_file")) {
            return 'Internal Error Saving';
        }
        if (!unlink(get_picture_file($old_picture['file'], $old_picture['extension']))) {
            error_log("Problem deleting old file $old_picture[file].$old_picture[extension]          Continuing...\n");
        }
    }
    error_log("updating with " . var_export($picture_data['name'], true) . ", " . var_export($file_number, true) . ", " .
        var_export($new_extension, true) . ", " . var_export($picture_data['description'], true) . ", " .
        var_export($old_picture['file'], true) . "\n");
    $st = $db->prepare("update picture set name=?, file=?, extension=?, description=? where file=?;");
    if (!$st->execute(array($picture_data['name'], $file_number, $new_extension, $picture_data['description'], $old_picture['file']))) {
        if (!unlink("$new_file")) {
            error_log("Error deleting $new_file in edit.\n");
        }
        error_log("Error executing query. deleted new file $new_file\n");
        return 'Internal Error Writing';
    }
    return null;
}

function delete_picture($picture_data) {
    error_log("Deleting picture " . var_export($picture_data['picturenum'], true) . "\n");
    if ($picture_data['picturenum'] === null || strlen($picture_data['picturenum']) === 0) {
        return 'Invalid picture to delete';
    }
    $old_picture = get_picture($picture_data['picturenum']);
    if ($old_picture === null) {
        return "Invalid picture to delete";
    }
    if (null === ($db = get_connection())) {
        return "Internal Error Connecting";
    }
    if (!unlink(get_picture_file($old_picture['file'], $old_picture['extension']))) {
        error_log("Error deleting old file $old_picture[file].");
        return 'Internal Error Deleting';
    }
    $st = $db->prepare("delete from picture where file=?;");
    if (!$st->execute(array($old_picture['file']))) {
        error_log("Error executing query.\n");
        return 'Internal Error Recording Delete';
    }
    return null;

}

function get_picture($picture_num) {
    error_log("Getting picture from picture num " . var_export($picture_num, true) . "\n");
    if (null === ($db = get_connection())) {
        return null;
    }
    $st = $db->prepare("select name, file, extension, description from picture where file = ?;");
    if (!$st->execute(array($picture_num))) {
        error_log("Failed to execute query on picture num $picture_num\n");
        return null;
    }
    $ret = $st->fetchAll();
    if (!array_key_exists('0', $ret)) {
        return null;
    }
    return $ret['0'];
}
?>
