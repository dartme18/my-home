<?
require('include/auth.php');
if (!is_authenticated()) {
    show_auth_page();
}
require('include/picture_management.php');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!isset($_FILES) || !isset($_FILES['picture'])) {
        $error = 'No file provided.';
    }
    $error = add_picture($_POST['name'], $_POST['description']);
    if ($error === null) {
        $success = "Successfully added picture";
    }
}
?>
<html>
    <head>
        <title><?=$website_name?></title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="new-page-parent">
            <h1>Add Picture</h1>
            <hr>
            <a href="admin.php">Admin</a>
            <form class="new-page-form" method="post" enctype="multipart/form-data">
                <input class="text-input new-page-input" placeholder="Name" name="name" type="text"/>
                <input class="new-page-file" id="picture-upload" name="picture" type="file"/>
                <input class="button-input" value="Add Picture" type="submit"/>
            </form>
            <? if ($error !== null) echo('<p class="new-page-error fade-out-5s">' . $error . "</p>"); ?>
            <? if ($success !== null) echo('<p class="new-page-success fade-out-5s">' . $success . "</p>"); ?>
        </div>
    </body>
</html>
