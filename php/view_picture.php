<?
require('include/picture_management.php');
$picture = get_picture($_GET['picture']);
$error = null;
if ($picture === null) {
    $error = "Invalid picture.";
}
?>
<html>
    <head>
        <title><?=$website_name?></title>
        <link rel="stylesheet" href="style.css">
        <? if ($error === null) { ?>
        <style>html { background: url(<?=get_picture_link($picture)?>) no-repeat center center fixed; background-size: contain; }</style>
        <? } else { ?>
        <div><?=$error ?></div>
        <? } ?>
    </head>
    <body>
        <h1><?=htmlspecialchars($picture['name'])?></h1>
        <h3><?=$picture['description'] ? $picture['description'] : 'no description'?></h1>
    </body>
</html>
