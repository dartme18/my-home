<?
require('include/auth.php');
if (!is_authenticated()) {
    show_auth_page();
}
require('include/picture_management.php');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_POST['save'])) {
        $error = edit_picture($_POST);
        if ($error === null) {
            $success = "Successfully saved picture";
        }
    } else if (isset($_POST['delete'])) {
        $error = delete_picture($_POST);
        if ($error === null) {
            $success = "Successfully deleted picture";
        }
    }
    $picture = get_picture($_POST['picturenum']);
    header("Location: edit_picture.php?picture=$picture[file]");
    exit(302);
} else {
    $picture = get_picture($_GET['picture']);
}
if ($picture === null) {
    $error = "Invalid picture";
}
?>
<html>
    <head>
        <title><?=$website_name?></title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="new-page-parent">
            <h1>Edit Picture</h1>
            <hr>
            <a href="admin.php">Admin</a>
            <form class="new-page-form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="picturenum" value="<?=$picture['file']?>">
                <input class="text-input new-page-input" placeholder="Name" name="name" type="text" value="<?=$picture['name']?>">
                <input class="new-page-file" id="picture-upload" name="picture" type="file"/>
                <div class="button-group">
                    <input class="button-input" value="Save" name="save" type="submit"/>
                    <input class="button-input" value="Delete" name="delete" type="submit"/>
                </div>
                <? if ($success !== null) echo('<p class="new-page-success fade-out-5s">' . $success . "</p>"); ?>
                <? if ($error !== null) echo('<p class="new-page-error">' . $error . "</p>"); ?>
                <img src="<?=get_picture_link($picture)?>" class="edit-view-image"/>
            </form>
        </div>
    </body>
</html>
