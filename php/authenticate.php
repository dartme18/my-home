<?php
require('include/auth.php');

$authenticated = is_authenticated();
$failed_attempt = false;
if (!$authenticated && $_SERVER['REQUEST_METHOD'] === 'POST') {
    $attempt = $_POST['password'];
    if (try_authenticate($attempt)) {
        $authenticated = true;
    } else {
        $failed_attempt = true;
    }
}
if ($authenticated) {
    header('Location: admin.php');
    exit (0);
}
?>
<html><head><title><?=$websitetitle?> - Authenticate</title></head>
<body>
<form method="post">
<div style="display: flex; align-items: center; flex-direction: column;">
    <div style="font-size: x-large; padding: 1em;">
        Welcome
    </div>
<div style="font-size: large; padding: .75em;">
Enter the administrator passphrase.
</div>
    <input name="password" type="password" placeholder="passphrase" style="padding: .5em; margin: 1em;" />
<?php
    if ($failed_attempt) {
?>
<div style="padding: 1em; color: red;">
Password did not match.
</div>
<?php
    }
?>
    <input type="submit" name="submit-button" value="Submit" style="padding: .5em; margin: .5em;" />
</div>
</form>
</body>
</html>
